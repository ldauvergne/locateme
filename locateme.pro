# Add more folders to ship with the application, here
folder_01.source = src/qml
folder_01.target = src

folder_02.source = src/javascript
folder_02.target = src

folder_03.source = src/images
folder_03.target = src

DEPLOYMENTFOLDERS = folder_01 folder_02 folder_03

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =


# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable
CONFIG += shareuiinterface-maemo-meegotouch share-ui-plugin share-ui-common


# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += \
    src/sharestring.cpp \
    src/main.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    src/javascript/UIConstants.js \
    src/javascript/Storage.js \
    src/javascript/SaveandLoad.js \
    src/javascript/LocationRequest.js \
    src/javascript/GoogleLocationAPI.js \
    src/javascript/APSettings.js \
    src/qml/WorldPage.qml \
    src/qml/TimesPage.qml \
    src/qml/SwitchItem.qml \
    src/qml/SettingsPage.qml \
    src/qml/MainPage.qml \
    src/qml/main.qml \
    src/qml/GroupSeparator.qml \
    src/qml/DisclaimerPage.qml \
    src/qml/CMWTimezoneChooser.qml \
    src/qml/CMWHeader.qml \
    src/qml/CMWDateChooser.qml \
    src/qml/AnimationWaiting.qml \
    src/qml/AboutPage.qml \
    src/qml/Background.qml

HEADERS += \
    src/sharestring.h
