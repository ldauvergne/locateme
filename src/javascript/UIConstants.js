/**********************************************************************
 * Copyright 2012 Leopold Dauvergne
 *
 * This file is part of Clock My World.
 *
 * Clock My World is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Clock My World is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clock My World. If not, see <http://www.gnu.org/licenses/>
 *
 ***********************************************************************/

.pragma library

var FONT_FAMILY = "Nokia Pure Text";

var FONT_XXLARGE  = 45;
var FONT_XLARGE   = 41;
var FONT_LARGE    = 37;
var FONT_SLARGE   = 33;
var FONT_DEFAULT  = 29;
var FONT_LSMALL   = 25;
var FONT_SMALL    = 21;
var FONT_XSMALL   = 17;
var FONT_XXSMALL  = 12;

var PADDING_XSMALL  = 2;
var PADDING_SMALL   = 4;
var PADDING_MEDIUM  = 6;
var PADDING_LARGE   = 8;
var PADDING_DOUBLE  = 12;
var PADDING_XLARGE  = 16;
var PADDING_XXLARGE = 24;



/* InfoBanner properties */
var INFOBANNER_BORDER_MARGIN = 10;
var INFOBANNER_OPACITY = 0.9
var INFOBANNER_LETTER_SPACING = -1.2
var INFOBANNER_DISMISS_TIMER = 3000
var INFOBANNER_ANIMATION_DURATION = 200

var SIZE_BUTTON = 51;
var SIZE_SMALL_BUTTON = 43;
var WIDTH_SMALL_BUTTON = 122;
var WIDTH_TUMBLER_BUTTON = 222;

var FONT_BOLD_BUTTON = true;
