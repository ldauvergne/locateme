#include <QtGui/QApplication>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QDeclarativeComponent>
#include <QtDeclarative>

#include "qmlapplicationviewer.h"
#include "sharestring.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QScopedPointer<QmlApplicationViewer> viewer(QmlApplicationViewer::create());


    // Share item
    ShareString shareString;
    viewer->rootContext()->setContextProperty("shareString", &shareString);




    // Start app
    viewer->setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);
    viewer->setMainQmlFile(QLatin1String("src/qml/main.qml"));
    viewer->showExpanded();

    return app->exec();
    /*
    QScopedPointer<QApplication> app(createApplication(argc, argv));


    QmlApplicationViewer viewer;

    // Share item
    ShareString shareString;
    viewer.rootContext().setContextProperty("shareString", &shareString);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);
    viewer.setMainQmlFile(QLatin1String("qml/locateme/main.qml"));
    viewer.showExpanded();

    return app->exec();*/
}
