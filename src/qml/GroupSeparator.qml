import QtQuick 1.1
import com.nokia.meego 1.0


Item {
    id:root
    property string title
    property string color:"white"
    width: parent.width
    height: sortingLabel.height
    clip: true

    Rectangle {
        id: sortingDivisionLine
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 0
        anchors.left: parent.left
        anchors.rightMargin: 20
        anchors.right: sortingLabel.left
        height: 1
        color: "white"
        opacity: 0.3
    }

    Rectangle {
        anchors.top: sortingDivisionLine.bottom
        anchors.left: sortingDivisionLine.left
        width: sortingDivisionLine.width
        height: 1
        color: "darkgray"
        opacity: 0.3
    }

    Text {
        id: sortingLabel
        text: title
        font.pointSize: 20
        font.bold: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.topMargin: 28
        anchors.right: parent.right
        anchors.rightMargin: 0
        color: root.color
        opacity: 1
    }

}
