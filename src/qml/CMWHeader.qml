// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import "../javascript/UIConstants.js" as UIConstants

Rectangle{
    property alias title:pageTitle.text
    color:"transparent"
    clip: true


    Image{
        id:clockmyworld_icon
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 10
        height:70//parent.height
        width:height
        smooth: true
        source:'../images/locateme80.png'
        fillMode: Image.PreserveAspectFit
    }

    Item{
        anchors{
            right:parent.right
            left:clockmyworld_icon.right
            top:parent.top
            bottom: parent.bottom
        }

        Text{
            id: pageTitle
            color:"white"
            style: Text.Raised
            font.family: UIConstants.FONT_FAMILY
            font.pixelSize: UIConstants.FONT_XXLARGE+10
            anchors.centerIn: parent

        }
    }


    Rectangle {
        height: 1
        width: parent.width
        x:0; y: 81
        color: "gray"
        opacity: 0.6
    }
    Rectangle {
        height: 1
        width: parent.width
        x:0; y: 82
        color: "lightgray"
        opacity: 0.8
    }
}
