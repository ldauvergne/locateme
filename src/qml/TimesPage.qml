// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

import "../javascript/UIConstants.js" as UIConstants
import "../javascript/APSettings.js" as APSettings
import "../javascript/Storage.js" as Storage
import "../javascript/GoogleLocationAPI.js" as GoogleLocationAPI


Item{

    property string locationtown:""
    property string locationstreet:"null"


    CMWHeader{
        id:top
        title: qsTr("Clock Me")
        anchors.top:parent.top
        width:parent.width
        height: 83
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom
        height: parent.height  - top.height - toolbar.height
        width: parent.width
        contentWidth: width
        contentHeight: column.height
        clip: true

        Column {
            id: column
            anchors { top: parent.top; left: parent.left; right: parent.right; margins: 16 }
            spacing: 10

            GroupSeparator {
                title: qsTr("My time")
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Sunrise time  : "+mainpage.mysunrise)
                height: 30
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Sunset time  : "+mainpage.mysunset)
                height: 30
            }

            GroupSeparator {
                title: qsTr("My location")
            }

            Item{
                id:mylocation
                height:30
                width: parent.width

                AnimationWaiting {
                    visible: mainpage.locationstreet=="null" ? true: false
                    working: mainpage.locationstreet=="null" ? true: false

                }

                Text {
                    id:gpslocationdata
                    color: "white"
                    visible: mainpage.locationstreet=="null" ? false: true
                    text: mainpage.locationtown + "\n" + mainpage.locationstreet
                    font.pointSize: UIConstants.FONT_SMALL

                }

            }



        }

    }

    Item{
        anchors{
            left:parent.left
            right:parent.right

            bottom: toolbar.top
            bottomMargin: 10
        }
        height: 20
        Label {
            anchors.centerIn: parent
            color:"white"
            text: "<- Use screen area to slide ->"
            height: 20
        }
    }

    InfoBanner {
        id:bannerrefresh
        text: "Refresh in progress"
        iconSource: "../images/Refresh.png"
        z:2
    }
    // define the menu
    Menu {
        id: mainMenu
        content: MenuLayout {
            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }

        }
    }

    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom
        }
        tools: ToolBarLayout {

            ToolIcon {
                iconId: "toolbar-tab-previous"
                onClicked: view.currentIndex=0
            }

            ToolIcon {
                iconId: "toolbar-refresh2"
                onClicked:  {
                    mainpage.locationstreet="null"
                    mainpage.locationtown="null"
                    GoogleLocationAPI.load(mylatitude,mylongitude)
                    bannerrefresh.show()
                }
            }

            ToolIcon {
                iconId: "toolbar-share"
                onClicked: {
                    shareString.title="Clock my World ! N9"
                    shareString.description=qsTr("Clock my World ! : My Sunrise and Sunset times are "+ mysunrise + " - " + mysunset + " and I'm currently in "+mainpage.locationstreet+", "+mainpage.locationtown+".")
                    shareString.mimeType="text/x-url"
                    shareString.text="https://maps.googleapis.com/maps/api/staticmap?center={"+mainpage.locationstreet+","+mainpage.locationtown+"}&zoom=14&size=400x400&maptype=hybrid&sensor=false"
                    shareString.share();
                }
            }

            ToolIcon {
                iconId: "toolbar-view-menu"
                onClicked: mainMenu.open()
            }
            ToolIcon {
                iconId: "toolbar-tab-next"
                onClicked: view.currentIndex=2
            }



        }
    }





}
