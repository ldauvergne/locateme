// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import QtMobility.location 1.1


import "../javascript/UIConstants.js" as UIConstants
import "../javascript/APSettings.js" as APSettings
import "../javascript/Storage.js" as Storage
import "../javascript/SaveandLoad.js" as SaveandLoad
import "../javascript/LocationRequest.js" as LocationRequest
import "../javascript/GoogleLocationAPI.js" as GoogleLocationAPI


Page{
    id: mainpage
    orientationLock: PageOrientation.LockPortrait

    state: "MAINBROWSER"



    property double mylongitude: SaveandLoad.getCoordinate("longitude")
    property double mylatitude: SaveandLoad.getCoordinate("latitude")

    property string mysunrise : "Not loaded"
    property string mysunset : "Not loaded"

    property double otherlongitude
    property double otherlatitude

    property string othersunrise : "Not loaded"
    property string othersunset : "Not loaded"


    property string locationstreet :"null"
    property string locationtown :"null"

    property bool saveposition:Storage.getSetting("SAVE_LOCATION")
    property bool savedate:Storage.getSetting("SAVE_DATE")
    property bool savetz:Storage.getSetting("SAVE_TIMEZONE")

    PositionSource {
        id: positionSource
        updateInterval: 1500
        active: !saveposition
        onPositionChanged: {
            mylongitude= positionSource.position.coordinate.longitude
            mylatitude= positionSource.position.coordinate.latitude

            mysunrise= LocationRequest.gettime(mylatitude,mylongitude,1)
            mysunset= LocationRequest.gettime(mylatitude,mylongitude,0)
        }

    }

    Item {
        id: datePerso
        Component.onCompleted: {
            if (Storage.getSetting("SAVE_DATE")=="false"){datePerso.dateperso= new Date();}
            else{datePerso.dateperso= new Date((new Date()).getFullYear(),Storage.getSetting("SAVEDATE_MONTH"),Storage.getSetting("SAVEDATE_DAY"))}

            if (Storage.getSetting("SAVE_TIMEZONE")=="false"){datePerso.timezone= (-(datePerso.dateperso.getTimezoneOffset()/60))}
            else{datePerso.timezone= Storage.getSetting("MY_TIMEZONE")}

        }
        property date dateperso
        property int timezone

    }

    Background{
        id:backgroundImage
    }

    Rectangle{
        id: mainbrowser
        opacity:1
        color:"transparent"
        anchors{
            fill: parent
        }


        ////////////////// MISE EN PLACE DES VUES
        VisualItemModel {
            id: itemModel

            //First hub tile
            SettingsPage{
                width:mainpage.width
                height:mainpage.height
            }


            //Second hub tile
            TimesPage{
                width:mainpage.width
                height:mainpage.height


            }

            //Third hub page
            WorldPage{
                id:worldpage
                width:mainpage.width
                height:mainpage.height
            }

        }

        ////////////////// GESTION DES VUES
        ListView {
            id: view
            visible:true
            property int bigheight: pagestackwindow.width//-toolbar.height

            model: itemModel
            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem;
            flickDeceleration: 500
            cacheBuffer: 2000;

            boundsBehavior: Flickable.DragOverBounds

            property int previousX;

            x: 0

            width: mainpage.width
            height: bigheight
            contentWidth: 0


            onMovementStarted: previousX=contentX
            onContentXChanged:{ if ((view.contentX >= worldpage.x) && (view.contentX <=(worldpage.x+worldpage.width-30)))
                {
                    if (previousX>contentX && contentX<(worldpage.x+50)) {
                        setSnapMode(true);
                    }else {
                        setSnapMode(false);
                    }
                } else {
                    setSnapMode(true);
                }
                backgroundImage.ximage=((0-view.contentX)/2)-200;
            }


            function setSnapMode(on){
                if (on===true){
                    view.snapMode=ListView.SnapOneItem;
                    view.highlightRangeMode= ListView.StrictlyEnforceRange;
                } else if (on===false) {
                    view.snapMode=ListView.NoSnap;
                    view.highlightRangeMode= ListView.NoHighlightRange;
                }
            }
            //Component.onCompleted: currentIndex=1
        }
    }
    CMWDateChooser{
        id: datechooserpage
        anchors.fill: parent
        opacity: 0
    }
    CMWTimezoneChooser{
        id: tzchooserpage
        anchors.fill: parent
        opacity: 0
    }



    states: [
        State {
            name: "MAINBROWSER"
            PropertyChanges { target: mainbrowser; opacity:1}
            PropertyChanges { target: datechooserpage; opacity:0}
            PropertyChanges { target: tzchooserpage; opacity:0}
        },
        State {
            name: "DATECHOOSERPAGE"
            PropertyChanges { target: datechooserpage; opacity:1}
            PropertyChanges { target: mainbrowser; opacity:0}
            PropertyChanges { target: tzchooserpage; opacity:0}
        }
        ,
        State {
            name: "TZCHOOSERPAGE"
            PropertyChanges { target: tzchooserpage; opacity:1}
            PropertyChanges { target: mainbrowser; opacity:0}
            PropertyChanges { target: datechooserpage; opacity:0}
        }
    ]

    transitions: [
        Transition {
            to: "MAINBROWSER"
            PropertyAnimation  { target: mainbrowser; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [datechooserpage,tzchooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        },
        Transition {
            to: "DATECHOOSERPAGE"
            PropertyAnimation  { target: datechooserpage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [mainbrowser,tzchooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        }
        ,
        Transition {
            to: "TZCHOOSERPAGE"
            PropertyAnimation  { target: tzchooserpage; property: "opacity"; to: 1.0; duration: 300}
            PropertyAnimation  { targets: [mainbrowser,datechooserpage]; property: "opacity"; to: 0.0; duration: 300;easing.type: Easing.InOutQuad}
        }
    ]
    Timer{
        running:true
        interval: 50
        onTriggered:{
            mainpage.mysunrise=LocationRequest.gettime(mylatitude,mylongitude,1)
            mainpage.mysunset=LocationRequest.gettime(mylatitude,mylongitude,0)
            GoogleLocationAPI.load(mylatitude,mylongitude)
            view.currentIndex=1
        }
    }
}
