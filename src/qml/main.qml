/**********************************************************************
 * Copyright 2012 Leopold Dauvergne
 *
 * This file is part of Clock My World.
 *
 * Clock My World is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Clock My World is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Clock My World. If not, see <http://www.gnu.org/licenses/>
 *
 ***********************************************************************/



import QtQuick 1.1
import com.nokia.meego 1.0



import "../javascript/Storage.js" as Storage
import "../javascript/APSettings.js" as APSettings


PageStackWindow {
    id: pagestackwindow
    Component.onCompleted: {

        Storage.initialize();

        if (Storage.getSetting("VERSION")!=APSettings.CLOCKMYWORLD_VERSION){
            Storage.eraseTable();
            Storage.initialize();
            Storage.setSetting("VERSION",APSettings.CLOCKMYWORLD_VERSION)
            console.log("LOG: Table erased, new version :"+APSettings.CLOCKMYWORLD_VERSION)
        }


        console.log("-"+Storage.getSetting("HIDE_DISCLAIMER"))

        if(Storage.getSetting("HIDE_DISCLAIMER")=="false"){
            return pageStack.push(Qt.resolvedUrl("DisclaimerPage.qml"))
        }
        else{
            return pageStack.push(Qt.resolvedUrl("MainPage.qml"))
        }
    }



}
/*
PageStackWindow {
    id:pagestackwindow
    showStatusBar: true
    initialPage: window
    property bool disclaimerok: false


    Page{
        id: window
        orientationLock: PageOrientation.LockPortrait

        Rectangle{
            anchors.fill: parent
            color: "#000000"
            Image{
                id:backgroundImage
                x: 0
                z: 0
                opacity: 0.6
                height: parent.height
                rotation: 0
               // source: "wallplocateme.jpg" //ImageLoader.getRandomImage()

            }
            Component.onCompleted:  {
                // splash is already rendered on the screen
                // user is looking on splash
                // now we can start loader to load main page
                Storage.initialize();
                mainLoader.source=loadmain()
            }
            function loadmain(){
                if(SaveandLoad.saveget("DisclaimerOK")=="true"){return "MainPage.qml"}
                else{return "Disclaimer.qml"}
            }
        }

        Loader { // this component performs deferred loading.
            id: mainLoader

        }



    }

}*/
