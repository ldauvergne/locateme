// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

import "../javascript/UIConstants.js" as UIConstants
import "../javascript/APSettings.js" as APSettings
import "../javascript/Storage.js" as Storage
import "../javascript/GoogleLocationAPI.js" as GoogleLocationAPI
import "../javascript/LocationRequest.js" as LocationRequest

Item{

    property string locationtown: "null"
    property string locationstreet:"null"

    CMWHeader{
        id:top
        title: qsTr("My World")
        anchors.top:parent.top
        width:parent.width
        height: 83
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom
        height: parent.height - toolbar.height - top.height
        width: parent.width
        contentWidth: width
        contentHeight: column.height + 50
        clip: true

        Column {
            id: column
            anchors { top: parent.top; left: parent.left; right: parent.right; margins: 16 }
            spacing: 10

            GroupSeparator {
                title: qsTr("My time")
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("My sunrise time : "+ mainpage.mysunrise)
                height: 20
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("My sunset time  : "+mainpage.mysunset)
                height: 20
            }

            GroupSeparator {
                title: qsTr("Other time")
            }

            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Their sunrise time : "+mainpage.othersunrise)
                height: 25
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Their sunset time  : "+mainpage.othersunset)
                height: 25
            }

            GroupSeparator {
                title: qsTr("Set other time")
            }



            TextField {
                id: nomapslat
                width:parent.width
                placeholderText: "Latitude"
                validator: DoubleValidator{bottom: -90; top: 90;}
                inputMethodHints: Qt.ImhPreferNumbers
                focus: false
            }

            TextField {
                id: nomapslon
                width:parent.width
                placeholderText: "Longitude"
                validator: DoubleValidator{bottom: -180; top: 180;}
                inputMethodHints: Qt.ImhPreferNumbers
                focus: false
            }
        }

    }



    InfoBanner {
        id:bannerlat
        text: "Check your latitude"
        iconSource: "../images/Warning.png"

    }
    InfoBanner {
        id:bannerlon
        text: "Check your longitude"
        iconSource: "../images/Warning.png"

    }

    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom

        }
        tools: ToolBarLayout {
            id:worldpagetoolslayout
            opacity: 1
            anchors{
                fill:parent
                margins:10
            }

            ToolIcon {
                id:toolbarbuttonprevious
                iconId: "toolbar-tab-previous"
                onClicked: view.currentIndex=1
            }

            ButtonRow{

                anchors{
                    left:toolbarbuttonprevious.right
                    right:parent.right
                    margins:5

                }

                Button{
                    id: searchbutton
                    text:"Get times"
                    onClicked: {
                        if (parseFloat(nomapslat.text)>= -90 && parseFloat(nomapslat.text) <=90){

                            if (parseFloat(nomapslon.text)>= -180 && parseFloat(nomapslon.text) <=180){
                                mainpage.otherlatitude=parseFloat(nomapslat.text)
                                mainpage.otherlongitude=parseFloat(nomapslon.text)

                                mainpage.othersunrise= LocationRequest.gettime(otherlatitude,otherlongitude,1)
                                mainpage.othersunset= LocationRequest.gettime(otherlatitude,otherlongitude,0)
                            }
                            else{
                                bannerlon.show()}
                        }
                        else{
                            bannerlat.show()}

                    }
                }

                Button{
                    id: savebutton
                    text:"Make it own"

                    onClicked: {
                        if (parseFloat(nomapslat.text)>= -90 && parseFloat(nomapslat.text) <=90){

                            if (parseFloat(nomapslon.text)>= -180 && parseFloat(nomapslon.text) <=180){
                                mainpage.mylatitude=parseFloat(nomapslat.text)
                                mainpage.mylongitude=parseFloat(nomapslon.text)

                                mainpage.saveposition=true
                                Storage.setSetting("MY_LONGITUDE",mainpage.mylongitude)
                                Storage.setSetting("MY_LATITUDE",mainpage.mylatitude)

                                mainpage.mysunrise= LocationRequest.gettime(mylatitude,mylongitude,1)
                                mainpage.mysunset= LocationRequest.gettime(mylatitude,mylongitude,0)

                                GoogleLocationAPI.load(mylatitude,mylongitude)

                            }
                            else{
                                bannerlon.show()}
                        }
                        else{
                            bannerlat.show()}
                    }
                }
            }
        }
    }
}
