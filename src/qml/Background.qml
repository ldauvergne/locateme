// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle{
    anchors.fill: parent
    color: "#000000"
    property int ximage: 0

    Image{
        x: ximage
        z: 0
        opacity: 0.6
        height: parent.height
        rotation: 0
        source: "../images/wallplocateme.jpg" //ImageLoader.getRandomImage()

    }
}
