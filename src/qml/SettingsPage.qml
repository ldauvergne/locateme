import QtQuick 1.1
import com.nokia.meego 1.0

import "../javascript/UIConstants.js" as UIConstants
import "../javascript/APSettings.js" as APSettings
import "../javascript/Storage.js" as Storage
import "../javascript/LocationRequest.js" as LocationRequest
import "../javascript/GoogleLocationAPI.js" as GoogleLocationAPI


Item {


    CMWHeader{
        id:top
        title: qsTr("Settings")
        anchors.top:parent.top
        width:parent.width
        height: 83
    }

    Flickable {
        id: flickArea
        anchors.top: top.bottom
        height: parent.height - top.height - toolbar.height
        width: parent.width
        contentWidth: width
        contentHeight: column.height
        clip: true

        Column {
            id: column
            anchors { top: parent.top; left: parent.left; right: parent.right; margins: 10 }
            spacing: 10

            GroupSeparator {
                title: qsTr("Location")
            }

            SwitchItem {
                id: switchsaveloc
                title: qsTr("Save Location")
                check: saveposition
                onCheckChanged: {
                    Storage.setSetting("SAVE_LOCATION", value)
                    if(positionSource.active!==false){
                        Storage.setSetting("MY_LONGITUDE",mainpage.mylongitude)
                        Storage.setSetting("MY_LATITUDE",mainpage.mylatitude)
                        GoogleLocationAPI.load(mylatitude,mylongitude)
                    }
                    saveposition = value
                }
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Latitude : ")+mainpage.mylatitude
                height: 20
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: qsTr("Longitude : ")+mainpage.mylongitude
                height: 20
            }

            GroupSeparator {
                title: qsTr("Date")
            }
            SwitchItem {

                title: qsTr("Use own date")
                check: savedate

                onCheckChanged: {
                    if (Storage.getSetting("SAVE_DATE")=="false"){
                        mainpage.state="DATECHOOSERPAGE"

                    }
                    else{
                        datePerso.dateperso=new Date()
                        mysunrise= LocationRequest.gettime(mylongitude,mylatitude,1)
                        mysunset= LocationRequest.gettime(mylongitude,mylatitude,0)
                    }
                    Storage.setSetting("SAVE_DATE", value)
                    savedate=value
                }
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: "Day: "+ Qt.formatDateTime(datePerso.dateperso, "dd") + " Month: " + Qt.formatDateTime(datePerso.dateperso, "MM")
                height: 20
            }

            GroupSeparator {
                title: qsTr("Timezone")
            }

            SwitchItem {
                title: qsTr("Use own timezone")
                check: savetz
                onCheckChanged: {
                    if (Storage.getSetting("SAVE_TIMEZONE")=="false"){
                        mainpage.state="TZCHOOSERPAGE"
                    }
                    else{
                        datePerso.timezone= (-(datePerso.dateperso.getTimezoneOffset()/60))
                        mysunrise= LocationRequest.gettime(mylongitude,mylatitude,1)
                        mysunset= LocationRequest.gettime(mylongitude,mylatitude,0)
                    }
                    Storage.setSetting("SAVE_TIMEZONE", value)
                    savetz=value
                }
            }
            Label {
                verticalAlignment: Text.AlignBottom
                color:"white"
                text: "Timezone: "+ datePerso.timezone
                height: 20
            }
        }



    }

    ToolBar {
        id:toolbar

        anchors{
            bottom: parent.bottom
        }

        tools: ToolBarLayout {
            id: toolBarLayout

            ToolIcon {
                anchors{
                    right:parent.right
                }

                iconId: "toolbar-tab-next"
                onClicked: view.currentIndex=1
            }
        }
    }

}
